package fr.umfds.exo2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.Mockito.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;

@ExtendWith(MockitoExtension.class)
public class TestJeuDeCartes {
    // Inject Mock as a parameter if possible, @InjectMocks otherwise
    @Mock
    Random r;
    @InjectMocks
    JeuDeCartes jdc = new JeuDeCartes();

//    @Mock
//    JeuDeCartes jdcM = new JeuDeCartes();

//    // Could've also mocked the method itself, but mocking random makes more sense
//    @Test
//    void testNextIntWithValue51() throws  CarteIncorrecteException{
//        when(jdcM.pioche()).thenReturn(new Carte(13, Couleur.trefle));
//        Carte c = jdcM.pioche();
//        assertEquals(13,c.getHauteur());
//        assertEquals(Couleur.trefle,c.getCouleur());
//    }

    @Test
    void testNextIntWithValue51(){
        when(r.nextInt(52)).thenReturn(51);
        Carte c = jdc.pioche();
        assertEquals(13,c.getHauteur());
        assertEquals(Couleur.trefle,c.getCouleur());
    }

    @Test
    void testNextIntWithValue0(){
        when(r.nextInt(52)).thenReturn(0);
        Carte c = jdc.pioche();
        assertEquals(1,c.getHauteur());
        assertEquals(Couleur.pique,c.getCouleur());
    }

    @Test
    void testDistribution2Deck3Cards() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList();
        ArrayDeque<Carte> deck1 = new ArrayDeque();
        ArrayDeque<Carte> deck2 = new ArrayDeque();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck1.addLast(new Carte(3, Couleur.pique));

        deck2.addLast(new Carte(13, Couleur.trefle));
        deck2.addLast(new Carte(12, Couleur.trefle));
        deck2.addLast(new Carte(11, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(r.nextInt(anyInt())).thenReturn(0, 0, 0, 48, 47, 46);

        ArrayList<ArrayDeque<Carte>> result = jdc.distribution(3, 2);

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 3; j++) {
                Carte c1 = listDeckRef.get(i).pop();
                Carte c2 = result.get(i).pop();

                assertEquals(true, c1.getCouleur() == c2.getCouleur());
                assertEquals(true, c1.getHauteur() == c2.getHauteur());
            }
    }
}
