package fr.umfds.exo2;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.Mock;

import java.util.ArrayDeque;
import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class TestJeuBataille {
    @Mock
    JeuDeCartes mockJDC;

    @Test
    void testRoundSimple() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<>();

        ArrayDeque<Carte> deck1=new ArrayDeque<>();
        ArrayDeque<Carte> deck2=new ArrayDeque<>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(13, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

//        doReturn(listDeckRef).when(mockJDC).distribution(anyInt(), anyInt());
        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        Boolean result = jeuBataille.round();

        assertTrue(result);
        assertEquals(true, jeuBataille.jeuFini());
        assertEquals(2, jeuBataille.numJoueurGagnant());
    }

    @Test
    void testRound1Bataille() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(13, Couleur.coeur));

        deck2.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(10, Couleur.trefle));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        Boolean result = jeuBataille.round();

        assertTrue(result);
        assertEquals(true, jeuBataille.jeuFini());
        assertEquals(1, jeuBataille.numJoueurGagnant());
    }

    @Test
    void testRound2Bataille() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listeDeckRef = new ArrayList<>();

        ArrayDeque<Carte> ad1 = new ArrayDeque<>();
        ArrayDeque<Carte> ad2 = new ArrayDeque<>();

        ad1.addLast(new Carte(1, Couleur.pique));
        ad1.addLast(new Carte(1, Couleur.pique));
        ad1.addLast(new Carte(10, Couleur.pique));

        ad2.addLast(new Carte(1, Couleur.pique));
        ad2.addLast(new Carte(1, Couleur.pique));
        ad2.addLast(new Carte(13, Couleur.pique));

        listeDeckRef.add(ad1);
        listeDeckRef.add(ad2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listeDeckRef);

        JeuBataille jB = new JeuBataille(mockJDC);

        Boolean result =  jB.round();

        assertEquals(true, result);
        assertEquals(true, jB.jeuFini());
    }
//    void testRound2Bataille() throws Exception, CarteIncorrecteException {
//        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();
//
//        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
//        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();
//
//        deck1.addLast(new Carte(1, Couleur.pique));
//        deck1.addLast(new Carte(1, Couleur.pique));
//        deck1.addLast(new Carte(10, Couleur.coeur));
//
//        deck2.addLast(new Carte(1, Couleur.pique));
//        deck2.addLast(new Carte(1, Couleur.pique));
//        deck2.addLast(new Carte(13, Couleur.trefle));
//
//        listDeckRef.add(deck1);
//        listDeckRef.add(deck2);
//
//        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);
//
//        JeuBataille jeuBataille = new JeuBataille(mockJDC);
//
//        Boolean result = jeuBataille.round();
//
//        assertTrue(result);
//    }

    @Test
    void testRoundEarlyEnd() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(1, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        Boolean result = jeuBataille.round();

        assertTrue(result);
    }

    @Test
    void testNumJoueurGagnant2() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.numJoueurGagnant();

        assertEquals(2, result);
    }

    @Test
    void testNumJoueurGagnant1() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(1, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.numJoueurGagnant();

        assertEquals(1, result);
    }

    @Test
    void testNoNumJoueurGagnant() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.numJoueurGagnant();

        assertEquals(-1, result);
    }

    @Test
    void testTailleJeuJoueur1() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(1, Couleur.pique));
        deck1.addLast(new Carte(2, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.tailleJeuJoueur1();

        assertEquals(1, result);
    }

    @Test
    void testTailleJeuJoueur2() throws Exception, CarteIncorrecteException {
        ArrayList<ArrayDeque<Carte>> listDeckRef = new ArrayList<ArrayDeque<Carte>>();

        ArrayDeque<Carte> deck1 = new ArrayDeque<Carte>();
        ArrayDeque<Carte> deck2 = new ArrayDeque<Carte>();

        deck1.addLast(new Carte(2, Couleur.pique));
        deck1.addLast(new Carte(1, Couleur.pique));
        deck2.addLast(new Carte(2, Couleur.pique));

        listDeckRef.add(deck1);
        listDeckRef.add(deck2);

        when(mockJDC.distribution(anyInt(), anyInt())).thenReturn(listDeckRef);

        JeuBataille jeuBataille = new JeuBataille(mockJDC);

        jeuBataille.round();
        int result = jeuBataille.tailleJeuJoueur2();

        assertEquals(2, result);
    }
}
